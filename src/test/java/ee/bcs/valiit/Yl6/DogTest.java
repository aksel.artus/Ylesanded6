package ee.bcs.valiit.Yl6;

import ee.bcs.valiit.Yl6.Yl1.TestDog.Dog;
import ee.bcs.valiit.Yl6.Yl1.TestDog.GermanDog;
import ee.bcs.valiit.Yl6.Yl1.TestDog.JapaneseDog;
import ee.bcs.valiit.Yl6.Yl1.TestDog.RussianDog;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DogTest {

    private List<Dog> dogs;


    @Before
    public void setUp() {
        dogs = new ArrayList<>();
        dogs.add(new GermanDog());
        dogs.add(new JapaneseDog());
        dogs.add((new RussianDog()));


    }


    @Test
    public void testBark() {
        for (Dog dog : dogs) {
            String voice = dog.bark();
            if (dog instanceof GermanDog) {
                assertEquals("wuf-wuf", voice);
            } else if (dog instanceof JapaneseDog) {
                assertEquals("wan-wan; kian-kian", voice);

            } else if (dog instanceof RussianDog) {
                assertEquals("gaf-gaf", voice);
            }

        }
    }


}



