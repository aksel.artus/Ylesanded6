package ee.bcs.valiit.Yl6.Yl2;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateBasedEncryptor extends Cryptor{

    public DateBasedEncryptor(String filePath) throws IOException {
        super(filePath);
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        Map<String, String> dictionary = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int helper = year + month + day;
        for (String dictLine: fileLines) {
            String[] lineParts = dictLine.split(", ");
            int encoded = (int) lineParts[1].charAt(0) + helper;
            dictionary.put(lineParts[0], (String.valueOf(encoded) + ";"));
        }
        return dictionary;
    }
}
