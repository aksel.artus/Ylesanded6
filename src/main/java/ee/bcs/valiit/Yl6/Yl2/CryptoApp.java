package ee.bcs.valiit.Yl6.Yl2;

import java.io.IOException;

public class CryptoApp {

    public static void main(String[] args) {

        String envelope = "";
        // Mati kirjutab

        try {
            DateBasedEncryptor encryptor2 = new DateBasedEncryptor("C:\\Users\\opilane\\IdeaProjects\\cryptoapp\\src\\main\\resources\\alfabeet2.txt");
            String encText2 = encryptor2.translate("Tere tali");
            System.out.println("ENC: " + encText2);
            envelope = encText2;
        } catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda!");
        }

        try {
            DateBasedDecryptor decryptor2 = new DateBasedDecryptor("C:\\Users\\opilane\\IdeaProjects\\cryptoapp\\src\\main\\resources\\alfabeet2.txt");
            String decText2 = decryptor2.translate2(envelope);
            System.out.println("DEC: " + decText2);
        } catch (IOException e) {
            System.out.println("Võtmefaili ei suutnud lugeda!");
        }


    }
}
