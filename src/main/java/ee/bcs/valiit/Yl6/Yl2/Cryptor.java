package ee.bcs.valiit.Yl6.Yl2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> dictionary = null;

    public Cryptor(String filePath) throws IOException {
        List<String> fileLines = readAlphabet(filePath);
        this.dictionary = generateDictionary(fileLines);
    }

    protected List<String> readAlphabet(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.readAllLines(path);
    }

    protected abstract Map<String, String> generateDictionary(List<String> fileLines);

    public String translate(String text) {
        if (text != null) {
            StringBuilder result = new StringBuilder();
            char[] textChars = text.toCharArray();
            for (char c: textChars) {
                String letter = String.valueOf(c).toUpperCase();
                String symbol = dictionary.get(letter);
                result.append(symbol != null ? symbol: letter);
            }
            return result.toString();
        }
        return null;
    }

    public String translate2(String text) {
        if (text != null) {
            StringBuilder result = new StringBuilder();
            String[] textChars = text.split(";");
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int helper = year + month + day;
            for (String s: textChars) {
                if (s.length() > 4) {
                    String c = s.substring(0,1);
                    String sym = s.substring(1);
                    String symbol = dictionary.get(sym);
                    result.append(c).append(symbol);
                } else {
                    String symbol = dictionary.get(s);
                    result.append(symbol != null ? symbol: s);
                }
            }
            return result.toString();
        }
        return null;
    }
}
