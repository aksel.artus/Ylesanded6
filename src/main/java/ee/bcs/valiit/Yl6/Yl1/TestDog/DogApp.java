package ee.bcs.valiit.Yl6.Yl1.TestDog;

import java.util.ArrayList;
import java.util.List;

public class DogApp {

    public static void main(String[] args) {
        List<Dog> dogs = new ArrayList<>();
        dogs.add(new GermanDog());
        dogs.add(new JapaneseDog());
        dogs.add(new RussianDog());
        for (Dog dog : dogs) {
            dog.bark();
        }
    }
}
