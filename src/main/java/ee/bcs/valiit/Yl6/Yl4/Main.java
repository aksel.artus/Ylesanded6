package ee.bcs.valiit.Yl6.Yl4;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        FerrariF355 ferrariF355 = new FerrariF355();
        ferrariF355.setName("Ferraari F355");
        ferrariF355.setColor(" red");
        ferrariF355.setSpeed(200);
        FordFiesta fordFiesta = new FordFiesta();
        fordFiesta.setName("Ford Fiesta");
        fordFiesta.setColor(" blue");
        fordFiesta.setSpeed(140);
        RenaultMegane renaultMegane = new RenaultMegane();
        renaultMegane.setName("Renault Megane");
        renaultMegane.setColor(" grey");
        renaultMegane.setSpeed(100);

        List<Movable> movables = new ArrayList<>();
        movables.add(ferrariF355);
        movables.add(fordFiesta);
        movables.add(renaultMegane);
        for (Movable movable : movables) {
            System.out.println("Cars properties : \n name: " + movable.getName() +  "\n Color:" + movable.getColor() + "\n Speed: " + movable.getSpeed() + " km/h");
        }
    }
}

