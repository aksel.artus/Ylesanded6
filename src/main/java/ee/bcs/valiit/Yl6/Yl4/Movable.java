package ee.bcs.valiit.Yl6.Yl4;

public interface Movable {
    String getColor();

    void setColor(String color);

    int getSpeed();

    void setSpeed(int speed);

    void setName(String name);

    String getName();

    void drive();


}
