package ee.bcs.valiit.Yl6.Yl4;

import lombok.Data;

@Data
public abstract class Car implements Movable {

    private String color;
    private int speed;
    private String name;

    public abstract void drive();




}
